import './App.css';
import { BrowserRouter, Route, Routes,  } from 'react-router-dom';
import {lazy, Suspense} from "react"
import { Toaster } from 'react-hot-toast';
import { LoadUser } from './application/middlewares/auth';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import "@material-tailwind/react/tailwind.css";
import { MantineProvider } from '@mantine/core';


const Home = lazy(()=>import("./pages/MainPage/index.js"))
const Register = lazy(()=>import('./pages/Login/register.js'))
const Users = lazy (()=>import("./pages/Users"))
const Login = lazy (()=>import("./pages/Login/login.js"))
const Posts = lazy (()=>import("./pages/Posts/index.js"))
const MyPosts = lazy (()=>import("./pages/MyPosts/index.js"))
const Post = lazy (()=>import ("./pages/Post.js"))

function App() {
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(LoadUser());
  }, []);
  return (
    <BrowserRouter>
    <MantineProvider>
    <div className='px-24 pt-3 bg-blue-100 w-full  h-full h-screen '>
    <Suspense fallback={<p>asda</p>} >
      <Routes>
        <Route path =  "/" element = {<Home/>}/>
        <Route path =  "/posts" element = {<Posts/>}/>
        <Route path =  "/register" element = {<Register/>}/>
        <Route path =  "/login" element = {<Login/>}/>
        <Route path =  "/users" element = {<Users/>}/>
        {/* <Route path =  "/my-posts" element = {<PrivateRoute><MyPosts/></PrivateRoute>}/> */}
        <Route path =  "/my-posts" element = {<MyPosts/>}/>
        <Route path = "/post/:id" element = {<Post/>}/>
      </Routes>
      </Suspense>
    </div>
    <Toaster/>
    </MantineProvider>
    </BrowserRouter>
  );
}

export default App;
