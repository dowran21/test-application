import { useEffect, useState } from "react"
import { useDispatch } from "react-redux"
import { useLocation } from "react-router-dom"
import { get } from "../application/middlewares"
import NavBar from "../components/NavBar"

function Post (){
    const location = useLocation()
    const arr = location?.pathname?.split("/")
    const dispatch = useDispatch()
    const [post, setPost] = useState()
    useEffect(()=>{
        if(arr.length){
            dispatch(get({
                url:`/api/post/${arr[2]}`,
                action: (response) =>{
                    setPost(response.data?.rows)
                }
            }))
        }
    }, [])
    return (
        <>
        <NavBar/>
        <div className="flex flex-row justify-between items-center pt-5 pb-3"><p>{post?.name}</p> <p>{post?.user_name}</p> </div>
        <div dangerouslySetInnerHTML={{__html:post?.article}}> 
            
        </div>
        </>
    )
}

export default Post