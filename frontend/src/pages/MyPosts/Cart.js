import {useNavigate} from "react-router-dom"
// import {BiEdit} from "@react"
import { BiEdit } from "@react-icons/all-files/bi/BiEdit";
import { BiTrash } from "@react-icons/all-files/bi/BiTrash";
import { useDispatch } from "react-redux";
import { post } from "../../application/middlewares";


function Cart ({data, showForm, setData, token, setPosts}){
    const navigate = useNavigate()
    // console.log(data)
    const NavigateToPost = () => {
        navigate(`/post/${data?.id}`)
    }
    const dispatch = useDispatch()
    const deletePost = () =>{
        dispatch(post({
            url:`/api/user/delete-post/${data.id}`,
            token,
            action: (response) =>{
                if(response.success){
                    setPosts(prev => prev.filter(item => +item.id !== +data.id ))
                }
            }
        }))
    }
    return (
        <div className="p-3 cursor-pointer relative" onClick={()=>console.log("hello world")}>
            <div className="absolute  right-4 top-2 flex flex-row">
                <button onClick={()=>{setData(data); showForm(); }} className={`flex justify-center items-center w-10 h-10 mx-2 hover:text-white hover:bg-indigo-600 active:bg-white active:text-indigo-600 rounded-md focus:outline-none shadow-lg text-indigo-600 bg-white`}>
                    <BiEdit  className="text-2xl"/>
                </button>
                <button onClick={()=>deletePost()} className={`flex justify-center items-center w-10 h-10 mx-2 hover:text-white hover:bg-indigo-600 active:bg-white active:text-indigo-600 rounded-md focus:outline-none shadow-lg text-indigo-600 bg-white`}>
                    <BiTrash className="text-2xl"/>
                </button>
            </div>
            <div onClick={()=> {NavigateToPost();}}className="flex flex-col p-3 pt-8 hover:border-indigo-600 bg-gray-100 shadow-inner rounded-xl">
                {/* <div className="flex flex-row ">
                    <p>Имя пользователя:</p>
                    <p>{data?.name}</p>
                </div> */}
                <div className="flex flex-row ">
                    <p>Пост: </p>
                    <p>{data?.post_name} </p>
                </div>
                <div className="flex flex-row ">
                    <p>Дата</p>
                    <p>{data?.created_at} </p>
                </div>
            </div>
        </div>
    )
}

export default Cart;