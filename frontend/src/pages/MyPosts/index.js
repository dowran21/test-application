import { useEffect, useState } from "react"
import toast from "react-hot-toast"
import { useDispatch, useSelector } from "react-redux"
import { get } from "../../application/middlewares"
import NavBar from "../../components/NavBar"
import Cart from "./Cart"
import Form from "./Form"

function Posts (){
    const dispatch = useDispatch()
    const [posts, setPosts] = useState()
    const [count, setCount] = useState()
    const token = useSelector(state => state?.auth?.token)
    // const isLoading = useSelector(state)
    // console.log(token)
    useEffect(()=>{
        dispatch(get({
            url:'/api/user/get-posts',
            token,
            action: (response) =>{
                if(response.success){
                    console.log(response.data.rows)
                    setPosts(response?.data?.rows?.posts)
                    setCount(response?.data?.rows?.count)
                }else{
                    toast.error("Серверная ошибка")
                }
            }
        }))
    }, [])
    const [visible, setVisible] = useState(false)
    const [data, setData] = useState();
    const filterPosts = (id) =>{
        console.log(id)
        setPosts(posts.filter(item => +item?.id !== +id))
    }
    return (
        <>
            <NavBar/>
            <Form visible={visible} data = {data} setVisible = {setVisible} setPosts={setPosts} setCloseModal = {()=>setVisible(false)} token = {token}/>
            <div className="flex flex-col pt-4">
                <div className="flex flex-row justify-between items-between">
                    <p className="text-indigo-600 text-lg">Мои посты </p>
                    <button onClick={(e)=>{setVisible(true); setData({}) } } className="w-24 rounded-full border border-black hover:border-white  bg-blue-400 hover:bg-indigo-400 hover:text-white">Добавить</button>
                </div>
                <div className="grid grid-cols-2">
                    {posts?.map(item => <Cart key = {item?.id} token = {token} setData = {setData} setPosts = {setPosts} filterPost = {(id)=>filterPosts(id)} showForm = {()=>setVisible(true)}  data = {item}/> )}  
                </div>
            </div>
        </>
    )
}

export default Posts