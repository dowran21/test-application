import React, { useEffect, useState } from "react"

import Modal from "@material-tailwind/react/Modal";
import ModalHeader from "@material-tailwind/react/ModalHeader";
import ModalBody from "@material-tailwind/react/ModalBody";
import { RichTextEditor } from '@mantine/rte';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { post } from "../../application/middlewares";
import { axiosInstance } from "../../application/api";

function Form ({visible, setVisible, token, setCloseModal, data, setPosts}){
    const { register, handleSubmit, formState: { errors }, reset, setValue } = useForm({//setError
        resolver: yupResolver(schema),
    });
    // const [value, setValue] = useState()
    useEffect(()=>{
        if(data?.post_name){
            setValue('name', data?.post_name)
        }
    }, [visible])
    const dispatch = useDispatch()
    // const token = useSelector(state => state?.auth?.token)
    const onSubmit = (data2) =>{
        dispatch(post({
            url: data?.post_name ? `/api/user/update-post/${data.id}` : '/api/user/add-post',
            token,
            data:data2,
            action: (response) => {
                if(response.success){
                    // console.log("hello world")
                    if(!data?.post_name){
                        setPosts(prev => prev.concat(response?.data?.rows));
                        setVisible(false)
                        setValue("name", "")
                    }else{
                        setPosts(prev => prev.map(item => {if(item.id === data.id){
                            item.post_name = data?.name;
                            item.article = data?.article;
                        }return item}))
                        setVisible(false)
                        setValue("name", "")
                    }
                }
            }
 
        }))
    }
    const handleImageUpload = (file) =>
        new Promise((resolve, reject) => {
            const formData = new FormData();
            formData.append('picture', file);

            // fetch('http://localhost:5000/api/user/add-image', {
            // method: 'POST',
            // body: formData,
            // })
            // .then((response) => {console.log(response); response.json()})
            // .then((result) => {console.log(result);resolve(result.data?.url)})
            // .catch(() => reject(new Error('Upload failed')));
            const config = {
                onUploadProgress: function (progressEvent) {
                    // setProgress(Math.round((progressEvent.loaded * 100) / progressEvent.total));
                },
                headers: {
                    'Content-Type': 'multipart/form-data',
                    Authorization: `Bearer ${token}`
                },
            }
            const url = "/api/user/add-image"
            axiosInstance.post(url, formData, config).then(res => {
                resolve(res.data.url)
            }).catch(e => {
                console.log(e)
            })
        });
    return (
        <Modal size={"l"} active={visible} toggler={()=>setCloseModal()}>
            <ModalHeader toggler={()=>setCloseModal()}>
                <span className=" text-xl">{"Добавить пост"}</span>
            </ModalHeader>
            <ModalBody>
                <form onSubmit={handleSubmit(onSubmit)}>
                <div className="min-w-max w-full h-96flex flex-col justify-center items-center px-3 pt-5">
                <div className="relative w-full mb-6">
                    <label>Названия поста</label>
                    <input type="tel" {...register("name")}
                        autoComplete="off"
                        className={`${0 ? 'border-2 border-red-300 ring-red-100' : 'ring-indigo-600'} pl-2 shadow-inner h-10 w-full text-base bg-gray-50 rounded-md z-20 focus:bg-white focus:outline-none focus:ring-2 `}
                        placeholder="Hello post"
                    />
                    {/* <div className="absolute opacity-80 top-6 z-10 px-2 py-2 text-base font-medium text-gray-600 rounded-l h-10">
                        +993
                    </div> */}
                    <p className="absolute bottom-0 left-0 -mb-4 text-xs font-medium text-red-400">
                        {/* {errors?.email?.message} */}
                    </p>
                </div>
                <RichTextEditor value = {data?.article ? data?.article : "" } styles = {{display: 'flex', height: '500px'}} onChange={(val) => setValue("article", val)} onImageUpload={handleImageUpload}/>
                </div>
                {/* <div className="absolute bottom-12 lg:bottom-10 left-0 right-0 mx-auto"> */}
                    <div className="w-full flex justify-center items-center pt-3">
                    <button type="submit" disabled={0} className="w-40 flex remove-button-bg justify-center items-center px-4 h-10 text-white transform ease-in-out duration-300 hover:scale-110 active:scale-100 font-semibold text-base rounded-full bg-green-500 hover:bg-green-400 active:bg-green-500 focus:outline-none shadow-md">
                        {0 ?
                            <div className="w-12"><p size="sm" ></p></div>
                        :
                            'Добавить'
                        }
                    </button>
                    {/* </div> */}
                </div>
                </form>
            </ModalBody>
        </Modal>
    )
}

const schema = Yup.object().shape({
    name: Yup.string().min(8, "Минимум 8 значений").max(150, "Максимум 8 значений"),
    article: Yup.string().min(8, "Минимум 8 значений").required('Пароль обязателен')
});

export default Form