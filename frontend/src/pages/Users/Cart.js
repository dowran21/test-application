import {useNavigate} from "react-router-dom";


function Cart ({data}){
    const navigate = useNavigate()
    const NavigateUserPosts = () =>{
        navigate(`/posts?user_id=${data.id}`)
    }
    return (
        <div className="p-3 cursor-pointer" onClick={()=>NavigateUserPosts()}>
            <div className="flex flex-col p-3 hover:border-indigo-600 bg-gray-100 shadow-inner rounded-xl">
                <div className="flex flex-row ">
                    <p>Имя пользователя:</p>
                    <p>{data?.name}</p>
                </div>
                <div className="flex flex-row ">
                    <p>Почта пользователя</p>
                    <p>{data?.email} </p>
                </div>
            </div>
        </div>
    )
}

export default Cart