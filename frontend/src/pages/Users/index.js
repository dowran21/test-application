import { useEffect, useState } from "react"
import toast from "react-hot-toast"
import { useDispatch } from "react-redux"
import { get } from "../../application/middlewares"
import NavBar from "../../components/NavBar"
import Cart from "./Cart"

function Users (){
    const dispatch = useDispatch()
    const [users, setUsers] = useState()
    const [count, setCount] = useState()
    useEffect(()=>{
        dispatch(get({
            url:'/api/get-users',
            action: (response) =>{
                if(response.success){
                    // console.log(response.data.rows)
                    setUsers(response?.data?.rows?.users)
                    setCount(response?.data?.rows?.count)
                }else{
                    toast.error("Серверная ошибка")
                }
            }
        }))
    }, [])

    return (
        <>
            <NavBar/>
            <div className="flex flex-col">
                <p className="text-indigo-600 text-lg">Users </p>
                <div className="grid grid-cols-2">
                    {users?.map(item => <Cart key = {item?.id} data = {item}/> )}  
                </div>
            </div>
        </>
    )
}

export default Users