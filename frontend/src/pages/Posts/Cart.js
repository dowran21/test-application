import {useNavigate} from "react-router-dom"
// import {BiEdit} from "@react"



function Cart ({data, showForm, setData, token, setPosts}){
    const navigate = useNavigate()
    // console.log(data)
    const NavigateToPost = () => {
        navigate(`/post/${data?.id}`)
    }
    return (
        <div className="p-3 cursor-pointer relative">
            <div onClick={()=> {NavigateToPost();}}className="flex flex-col p-3 pt-8 hover:border-indigo-600 bg-gray-100 shadow-inner rounded-xl">
                <div className="flex flex-row justify-between items-center ">
                    <p>Имя пользователя: </p>
                    <p>{data?.name}</p>
                </div>
                <div className="flex flex-row justify-between items-center">
                    <p>Пост: </p>
                    <p>{data?.post_name} </p>
                </div>
                <div className="flex flex-row justify-between items-center ">
                    <p>Дата</p>
                    <p>{data?.created_at} </p>
                </div>
            </div>
        </div>
    )
}

export default Cart;