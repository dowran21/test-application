import { useEffect, useState } from "react"
import toast from "react-hot-toast"
import { useDispatch, useSelector } from "react-redux"
import { useLocation } from "react-router-dom"
import { get } from "../../application/middlewares"
import NavBar from "../../components/NavBar"
import Cart from "./Cart"

function Posts (){
    const dispatch = useDispatch()
    const location = useLocation()
    console.log(location)
    const [posts, setPosts] = useState()
    const [count, setCount] = useState()
    // const isLoading = useSelector(state)
    // console.log(token)
    useEffect(()=>{
        dispatch(get({
            url:`/api/get-posts${location?.search}`,
            action: (response) =>{
                if(response.success){
                    console.log(response.data.rows)
                    setPosts(response?.data?.rows?.posts)
                    setCount(response?.data?.rows?.count)
                }else{
                    toast.error("Серверная ошибка")
                }
            }
        }))
    }, [location?.search])
    const [visible, setVisible] = useState(false)
    const [data, setData] = useState();
    const filterPosts = (id) =>{
        console.log(id)
        setPosts(posts.filter(item => +item?.id !== +id))
    }
    return (
        <>
            <NavBar/>
            <div className="flex flex-col pt-4">
                <div className="flex flex-row justify-between items-between">
                    <p className="text-indigo-600 text-lg"> посты </p>
                </div>
                <div className="grid grid-cols-2">
                    {posts?.map(item => <Cart key = {item?.id}  data = {item}/> )}  
                </div>
            </div>
        </>
    )
}

export default Posts