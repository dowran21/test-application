import { useEffect, useState } from "react";
import {useLocation} from "react-router-dom";
import {Link} from "react-router-dom"
import {useDispatch, useSelector} from "react-redux"
import {Logout} from "../application/middlewares/auth"

function NavBar (){
    const location = useLocation()
    const [path, setPath] = useState()
    useEffect(()=>{
        setPath(location?.pathname)
    }, [location])
    const isLogged = useSelector(state => state?.auth?.isLogged)
    const dispatch = useDispatch()    
    return (

        <div className="flex flex-row px-10 justify-between items- text-white h-10 rounded-xl bg-indigo-600">
        <div className="flex flex-row justify-center items-center">
            <Link to = "/users" className={`pr-4 ${path === "/users" ? "underline" : ""} cursor-pointer hover:text-red-600`} >Пользователи</Link>
            <Link to = "/posts" className={`pr-4 ${path === "/posts" ? "underline" : ""} cursor-pointer hover:text-red-600`}>Посты</Link>
        </div>
        <div className="flex flex-row justify-center items-center">
            <Link to = "/" className={`pr-4 ${path === "/" ? "underline" : ""} cursor-pointer hover:text-red-600`}>Главная</Link>
        </div>
        <div className="flex flex-row justify-center items-center">
            {!isLogged ? 
            <> 
                <Link to = "/login" className={`pr-4 ${path === "/login" ? "underline" : ""} cursor-pointer hover:text-red-600`}>
                    Войти
                </Link>
                <Link to="/register" className={`pr-4 ${path === "/register" ? "underline" : ""} cursor-pointer hover:text-red-600`}>
                    Регистрация
                </Link>
            </>
            :
            <>
                <p onClick={()=>dispatch(Logout())} className={`pr-4 ${path === "/login" ? "underline" : ""} cursor-pointer hover:text-red-600`}>
                    Выйти
                </p>  
                <Link to = "/my-posts" className={`pr-4 ${path === "/my-posts" ? "underline" : ""} cursor-pointer hover:text-red-600`}>
                    Мои посты
                </Link>
            </>
            }
        </div>
        </div >
    )
}

export default NavBar