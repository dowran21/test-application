require("dotenv").config()
const JWT = require("jsonwebtoken")
const {status} = require("../utils/status")

const VerifyUserAccessToken = async (req, res, next) =>{
    let token = req.headers.authorization
    if (!token){
        return res.status(status.bad).send("Token not provided")
    }
    console.log(token)
    token = token.replace("Bearer ", "")
    JWT.verify(token, process.env.ACCESS_TOKEN_KEY, async (err, decoded) =>{
        if(err){
            console.log("I am in error")
            console.log(err)
            return res.status(status.forbidden).send("forbidden");
        }
        req.user = decoded;
        next()
    });
}

const VerifyUserRefreshToken = async (req, res, next) =>{
    let token = req.headers.authorization
    if (!token){
        return res.status(status.bad).send("Token not provided")
    }

    token = token.replace("Bearer ", "")
    JWT.verify(token, process.env.REFRESH_TOKEN_KEY, async (err, decoded) =>{
        if(err){
            console.log("I am in error")
            console.log(err)
            return res.status(status.forbidden).send("forbidden");
        }
        req.user = decoded;
        next()
    });
}

module.exports = {
    VerifyUserAccessToken,
    VerifyUserRefreshToken
}