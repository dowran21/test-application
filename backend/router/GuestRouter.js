const express = require("express")
const router = new express.Router()
const GuestController = require("../controllers/GuestController");

router.get('/get-users', GuestController.GetUsers)
router.get('/get-posts', GuestController.GetUserPosts)
router.get("/post/:id", GuestController.PostByID);

module.exports = router  