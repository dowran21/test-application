const express = require("express")
const router = new express.Router()
const UserController = require("../controllers/UserController");
const { VerifyUserAccessToken, VerifyUserRefreshToken } = require("../middlewares/AuthMiddleware");
const upload = require("../middlewares/upload");

router.post('/register', UserController.UserRegistration);
router.post("/login", UserController.UserLogin);
router.get('/load', VerifyUserRefreshToken, UserController.UserLoad);

router.post('/add-post', VerifyUserAccessToken, UserController.AddPost);
router.post('/delete-post/:id', VerifyUserAccessToken, UserController.DeletePost);
router.post('/update-post/:id', VerifyUserAccessToken, UserController.UpdatePost);
router.get('/get-posts', VerifyUserAccessToken, UserController.GetPosts)
router.post('/add-image', upload.single("picture"), UserController.AddPicture )

module.exports = router