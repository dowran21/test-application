DROP DATABASE IF EXISTS blog;

CREATE DATABASE blog;

\c blog;
  
CREATE TABLE users(
    id SERIAL PRIMARY KEY NOT NULL,
    email VARCHAR(150) NOT NULL,
    created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
    "name" VARCHAR (250),
    "password" VARCHAR(350),
    UNIQUE(email) 
);

CREATE TABLE posts (
    id SERIAL PRIMARY KEY NOT NULL,
    user_id INTEGER NOT NULL,
    article TEXT NOT NULL,
    "name" VARCHAR(150) NOT NULL,
    created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT now()
);