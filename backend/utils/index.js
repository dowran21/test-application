require("dotenv").config()
const JWT = require("jsonwebtoken")
const bcrypt = require("bcryptjs") //can be used bcrypt instead of bcryptjs

const GenerateUserAccessToken = async (data)=>{
    return JWT.sign(data, process.env.ACCESS_TOKEN_KEY , {expiresIn:"2h"})
}

const GenerateUserRefreshToken = async (data)=>{
    return JWT.sign(data, process.env.REFRESH_TOKEN_KEY, {expiresIn:"1d"})
}

const HashPassword = async (password) => {
    return bcrypt.hashSync(password, 5);
};

const ComparePassword = async (password, hash) =>{
    return bcrypt.compareSync(password, hash)
};

module.exports = {
    GenerateUserAccessToken,
    GenerateUserRefreshToken,
    HashPassword,
    ComparePassword
}