const {status} = require("../utils/status.js")
const UserHelper = require("../utils/index.js")
const database = require("../db/index.js")

const GetUsers = async (req, res) =>{
    const {page, search, limit} = req.query
    let offSet = ``
    if(page && limit) {
        offSet = `OFFSET ${page*limit} LIMIT ${limit}`
    }
    let wherePart = ``
    if(search) {
        wherePart += `AND (u.name ~* '${search}' OR u.email ~* '${search}')`
    }
    const query_text = `
        SELECT (
            SELECT COUNT(*) 
            FROM users u
            WHERE u.id > 0 ${wherePart}
        ), (
            SELECT json_agg(us) FROM (
                SELECT u.email, u.name, u.id, 
                    (SELECT COUNT(*) 
                    FROM posts p
                    WHERE p.user_id = u.id
                    ) AS post_count
                    FROM users u
                    WHERE u.id > 0 ${wherePart}
                ORDER BY id ASC
                ${offSet}
                
                
        )us) AS users 
    `
    try {
        const {rows} = await database.query(query_text, [])
        return res.status(status.success).json({rows:rows[0]})
    } catch (e) {
        console.log(e)
        console.log(query_text)
        return res.status(status.error).send(false)
    }
}

const GetUserPosts = async (req, res) =>{
    // const {id} = req.params
    const {page, limit, user_id} = req.query
    let offSet = ``
    if(page && limit) {
        offSet = `OFFSET ${page*limit} LIMIT ${limit}`
    } 
    let wherePart = ``
    if(user_id){
        wherePart += ` AND p.user_id = ${user_id}`
    }
    const query_text = `
    SELECT (
        SELECT COUNT(*) FROM posts p WHERE p.id > 0 ${wherePart}
        ), (
            SELECT json_agg(po) FROM (
                SELECT p.id, p.name AS post_name, p.article, to_char(p.created_at, 'DD.MM.YYYY') AS created_at
                , u.name
                FROM posts p 
                INNER JOIN users u
                    ON u.id = p.user_id
                WHERE p.id > 0 ${wherePart}
                ORDER BY created_at ASC
                ${offSet}
        )po) AS posts
    `
    try {
        const {rows} = await database.query(query_text, [])
        return res.status(status.success).json({rows:rows[0]})
    } catch (e) {
        console.log(e)
        return res.status(status.error).send(false)
    }
}

const PostByID = async (req, res) =>{
    const {id} = req.params
    console.log(id)
    const query_text = `
        SELECT p.name, p.article, to_char(p.created_at, 'DD.MM.YYYY') AS created_at, u.name AS user_name
        FROM posts p
        INNER JOIN users u
            ON u.id = p.user_id
        WHERE p.id = ${id}
    `
    try {
        const {rows} = await database.query(query_text, [])
        return res.status(status.success).json({rows:rows[0]})
    } catch (e) {
        console.log(e)
        return res.status(status.error).send(false)
    }
}

module.exports = {
    GetUsers,
    GetUserPosts,
    PostByID
}