const {status} = require("../utils/status.js")
const UserHelper = require("../utils/index.js")
const database = require("../db/index.js")

const UserRegistration = async (req, res) =>{
    const {email, name, password} = req.body;
    const hashPassword = await UserHelper.HashPassword(password)
    const query_text = `
        INSERT INTO users (name, email, password) VALUES ($1, $2, $3) RETURNING *
    `
    try {
        const {rows} = await database.query(query_text, [name, email, hashPassword])
        const data = {name:rows[0]?.name, email:rows[0].email, id:rows[0]?.id}
        const access_token = await UserHelper.GenerateUserAccessToken(data)
        const refresh_token = await UserHelper.GenerateUserRefreshToken(data)
        return res.status(status.success).json({access_token, refresh_token, data})
    } catch (e) {
        console.log(e)
        if(e.routine == "_bt_check_unique"){
            let message = {}
            message["email"] = `Такая почта уже существует`
            return res.status(status.conflict).send({error:message})
        }
        return res.status(status.error).send(false)
    }
}

const UserLogin = async (req, res) =>{
    const {email, password} = req.body
    const query_text = `
        SELECT * FROM users WHERE email = $1
    `
    try {
        const {rows} = await database.query(query_text, [email])
        if(!rows[0]){
            let message = {}
            message["email"] = `Проль неправильный`
            return res.status(status.conflict).send({error:message})
        }
        const compare = await UserHelper.ComparePassword(password, rows[0]?.password)
        if(compare){
            const data = {name:rows[0]?.name, email:rows[0].email, id:rows[0]?.id}
            const access_token = await UserHelper.GenerateUserAccessToken(data)
            const refresh_token = await UserHelper.GenerateUserRefreshToken(data)
            return res.status(status.success).json({access_token, refresh_token, data})
        }
    } catch (e) {
        console.log(e)
        return res.status(status.error).send(false)
    }
}

const UserLoad = async (req, res) =>{
    const {id} = req.user
    const query_text = `
        SELECT * FROM users WHERE id = ${id}
    `
    try {
        const {rows} = await database.query(query_text, [])
        const data = {name:rows[0]?.name, email:rows[0].email, id:rows[0]?.id}
        const access_token = await UserHelper.GenerateUserAccessToken(data)
        const refresh_token = await UserHelper.GenerateUserRefreshToken(data)
        return res.status(status.success).json({access_token, refresh_token, data})
    } catch (e) {
        console.log(e)
        return res.status(status.error).send(false)
    }
}

const AddPost = async (req, res) =>{
    const {name, article} = req.body
    console.log(name)
    console.log(article)
    const {id} = req.user
    const query_text = `
        INSERT INTO posts (user_id, article, name) VALUES ($1, $2, $3) 
            RETURNING name AS post_name, to_char(created_at, 'DD.MM.YYYY') AS created_at, id
    `
    try {
        const {rows} = await database.query(query_text, [id, article, name])
        return res.status(status.success).json({rows:rows[0]})
    } catch (e) {
        console.log(e)
        return res.status(status.error).send(false)
    }
}

const GetPosts = async (req, res) =>{
    const {id} = req.user
    const {page, limit} = req.query
    let OffSet = ``
    if(page && limit){
        OffSet = `OFFSET ${page*limit} LIMIT ${limit}`
    }
    const query_text = `
        SELECT (
            SELECT COUNT(*) FROM posts WHERE user_id = ${id}
            ), (
                SELECT json_agg(po) FROM (
                    SELECT id, name AS post_name, article, to_char(created_at, 'DD.MM.YYYY') AS created_at FROM posts WHERE user_id = ${id}
                    ORDER BY created_at ASC
                    ${OffSet}
            )po) AS posts 
        `
    try {
        const {rows} = await database.query(query_text, [])
        return res.status(status.success).json({rows:rows[0]})
    } catch (e) {
        console.log(e)
        return res.status(status.error).send(false)
    }
}

const UpdatePost = async (req, res) =>{
    const {id} = req.params;
    const {name, article} = req.body
    const query_text = `
        UPDATE posts SET name = $1, article = $2 WHERE id = $3
    `
    try {
        await database.query(query_text, [name, article, id])
        return res.status(status.success).send(true)
    } catch (e) {
        console.log(e)
        return res.status(status.error).send(false)
    }
}

const DeletePost = async (req, res) =>{
    const {id} = req.params
    const query_text = `
        DELETE FROM posts WHERE id = ${id}
    `
    try {
        await database.query(query_text, [])
        return res.status(status.success).send(true)
    } catch (e) {
        console.log(e)
        return res.status(status.error).send(false)
    }
}

const AddPicture = (req, res) =>{
    const file = req.file;
    console.log(req.file)
    return res.status(status.success).json({url:`http://141.136.44.10:7010/uploads/${file.filename}`})
}

module.exports = {
    UserRegistration,
    UserLogin,
    UserLoad,
    AddPost,
    GetPosts,
    UpdatePost,
    DeletePost,
    AddPicture

}